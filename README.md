# Innovation projects for sustainability

This repository is a template of documents to be written.

General planning: https://gitlab.com/BenOrcha/projet_innovation/blob/master/general%20planning.xlsx

Workshop 1:
 - product vision
 - stakeholders analysis
 - risk analysis
- link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%201%20-%20from%20idea%20to%20concept

Workshop 1: 
- organisation structure: team, roles (RACI matrix)
- link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%201%20-%20from%20idea%20to%20concept

Workshop 2 (beginning of the Business Model Canvas): 
- Customer segments: segmentation of target groups, profiling of target groups, constraints, choice of a target group for version 1
- Value proposition: needs – expectations – desires
- Key activity
- Key partners: partners, suppliers, competitors
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%202%20-%20from%20concept%20to%20project

Workshop 2:
- Technologies: alternatives, test, choice, traning, setup
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%202%20-%20from%20concept%20to%20project

Concept validation meeting:
- Pitch of the project
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/Concept%20validation%20meeting

Technical meeting 1:
- Functional diagram
- Product backlog and release planning
- Requirement analysis
- Design: technologies and framework, data model, algorithm, tooling 
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/technical%20meeting%201

Business plan meeting 1 (Business Model Canvas): 
- Revenues
- key resources
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/business%20plan%20meeting%201

Technical meeting 2:
- Sprint backlog
- Requirement analysis
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/technical%20meeting%202

Semester 7 validation:
- Pitch of the project
- Sprint review
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/semester%207%20validation

Technical meeting 3:
- Sprint backlog of the current sprint
- Requirement analysis of the current sprint
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/technical%20meeting%202

Technical meeting 4:
- Sprint backlog of the current sprint
- Requirement analysis of the current sprint
- Link: https://gitlab.com/BenOrcha/projet_innovation/tree/master/technical%20meeting%202

Who did what (RACI Matrix template): https://gitlab.com/BenOrcha/projet_innovation/blob/master/Responsibility%20assignment%20matrix%20-%20RACI%20for%20pre-studies.xlsx

Project indicators (to be modified if project location changes):
- who did what (RACI Matrix template): https://gitlab.com/BenOrcha/projet_innovation/blob/master/Responsibility%20assignment%20matrix%20-%20RACI%20for%20pre-studies.xlsx
- contributor's activity: https://gitlab.com/BenOrcha/projet_innovation/graphs/master
- charts: https://gitlab.com/BenOrcha/projet_innovation/graphs/master/charts

Follow this produre to create your own project based on this template :

- each team member must create a Gitlab account 
- create a public or private project under gitlab
- assign all team member to the project
- assign the project tutors to the project as reporter
- download this project template as a zip file or use the git pull command to download it
- edit the documents
- push them to your project (push git command)

For more information about git: https://git-scm.com/docs/gittutorial

